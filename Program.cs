﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;



namespace ClipboardSaver
{
    class Program
    {
        static void Main(string[] args)
        {

            System.IO.Directory.CreateDirectory("images");

            ClipboardReceiver.ClipboardChanged += delegate (Message m)
            {
                if (Clipboard.ContainsImage())
                {
                    Image image = Clipboard.GetImage();
                    image.Save("images\\image_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".png", System.Drawing.Imaging.ImageFormat.Png);
                }
            };
            ClipboardReceiver.Start();

            System.Windows.Forms.Application.Run();

            ClipboardReceiver.Stop();
        }
    }
}
