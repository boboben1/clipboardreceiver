﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace ClipboardSaver
{
    class ClipboardReceiver : Form
    {
        public delegate void ClipboardChangedDelegate(Message msg);
        public static event ClipboardChangedDelegate ClipboardChanged;
        private static ClipboardReceiver mInstance;

        bool first = true;


        protected IntPtr _next;

        ClipboardReceiver()
        {
            SetVisibleCore(true);
            _next = Invokes.SetClipboardViewer(mInstance.Handle);
        }

        public static void Start()
        {
            Thread t = new Thread(runForm);
            t.SetApartmentState(ApartmentState.STA);
            t.IsBackground = true;
            t.Start();
        }

        public static void Stop()
        {
            if (mInstance == null) throw new InvalidOperationException("ClipboardReceiver not Started!");
            ClipboardChanged = null;
            mInstance.Invoke(new MethodInvoker(mInstance.endForm));
        }

        protected override void SetVisibleCore(bool value)
        {
            if (mInstance == null) CreateHandle();
            mInstance = this;
            value = false;
            base.SetVisibleCore(value);
        }

        public static void runForm()
        {
            Application.Run(new ClipboardReceiver());
        }

        public void endForm()
        {
            Invokes.ChangeClipboardChain(this.Handle, _next);
            this.Close();
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == 0x0308)
            {
                if (!first)
                    ClipboardChanged?.Invoke(m);
                else first = false;
            }

        }
    }
}
